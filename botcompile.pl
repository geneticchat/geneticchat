#!/usr/bin/perl -n

while (1) {
	if (s/^\s*([\da-f]+)//) {
		my $hexdump = $1;
		print pack 'H*', $hexdump;
	}
	elsif (s/^\s*(['"])((?:[^\"]+|\\.)+)(??{ $1 })//) {
		my $lit = $2;
		$lit =~ s/\\(.)/$1/;
		print $lit;
	}
	elsif (s/^\s*\#.*$// or s/^\s+$//) {
		last
	}
	else {
		die "Unrecognized data: $_\n";
	}
}
