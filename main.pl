#!/usr/bin/perl
use strict;
use warnings;

BEGIN { unshift @INC, '.' }
use IRCBotInterp;

$SIG{PIPE} = sub {
	warn "SIGPIPE\n";
};

my $botctl =
new IRCBotInterp::RunMany;

sub botctlAdd {
	my ($bot) = @_;
	$botctl->add($bot);
}

if (not @ARGV) {
	die "Need bot filename(s)";
}

my %param;
$param{'limit'} = sub {
	my $limit = shift @ARGV;
	$botctl->limit($limit);
	warn "Bot limit set to $limit\n";
};

sub savedump;
while (@ARGV) {

my $botfn = shift;
	
	if ($botfn =~ s/^\-(\-)?//) {
		# Parameter(s)
		my @opts;
		if (defined $1) {
			@opts = $botfn;
		}
		else {
			@opts = split //, $botfn;
		}
		for my $opt (@opts) {
			my $F = $param{$opt};
			if (not defined $F) {
				undef $botctl;
				die "Parameter $opt not recognized"
			}
			$F->();
		}
		next
	}
	
	my $qty = 1;
	if ($botfn =~ s/^(\d+)\://) {
		$qty = $1;
	}
	
my $code = do
{
	open my $botF, '<', $botfn or die "Error opening $botfn";
	local $/;
	my $r = <$botF>;
	close $botF;
	$r
}
;

if ($code =~ m/^[\da-f]{8}\b/i) {
	warn "Is this a compiled botstrap? Looks like source...\n";
	sleep 1;
}

use Sys::Hostname;
	my $botnick = 'SOTF' . substr(hostname, 0, 2) . '$x';

	for my $i (1..$qty) {

my $bot =
new IRCBotInterp::Bot
	code => $code,
	trace => 1,
	idelay => 1,
	nick => $botnick,
	realname => $botfn,
		onClone  => \&botctlAdd,
		onCreate => \&botctlAdd,
		onDeath => \&savedump,
		forkmode => 'object',
;
		
	}
}

sub savedump {
	my ($bot) = @_;
	mkdir 'save';
	my $botnick = $bot->nick;
	my $savefile = "save/$botnick.save";
	open my $save, '>', $savefile or
		do {
			warn "Failed to open $savefile for writing, $botnick brain will be lost :(\n";
			warn "open error: $!\n";
			return
		};
	print $save $bot->dump;
	close $save;
}

END {
	return if not defined $botctl;
	for my $bot ($botctl->bots) {
		savedump($bot);
	}
};
$SIG{INT} = sub {
	exit 0;
};

$botctl->run;
