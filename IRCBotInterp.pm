package IRCBotInterp;
use strict;
use warnings;

my @verInfo = ('IRCBotInterp', 0, 2, 4);

{ our $i = 0 }

sub sortScoreAge {
	sort
	{
		my $c = $a->score <=> $b->score;
		return $c if $c;
		$a->startTime <=> $b->startTime
	}
	@_
}

sub sortScore {
	sort
	{ $a->score <=> $b->score }
	@_
}

package IRCBotInterp::RunMany;
use strict;
use warnings;

my $have_hires;
BEGIN {
eval <<\PERL;
	use Time::HiRes qw(sleep time);
PERL
	if ($@) {
		warn "Time::HiRes not installed. Sleeping will be inefficient (and probably non-existent).";
	}
	else {
		$have_hires = 1;
	}
}

use List::Util qw(min);

sub new {
	my $pkg = shift;
	bless {
		'newbots' => [@_],
	}, $pkg;
}

sub add {
	my $this = shift;
warn "ADD @_\n";
	push @{$this->{'newbots'}}, @_;
}

sub limit {
	my ($this, $nv) = @_;
	if (defined $nv) {
		$nv = undef unless $nv;
		return
		$this->{'limit'} = $nv;
	}
	$this->{'limit'}
}

sub bots {
	my ($this) = @_;
	(@{$this->{'bots'}})
}

sub getTimer {
	my ($this) = @_;
	min
	grep { defined }
	map { $_->getTimer }
	$this->bots;
}

# NOTE: exact copy of bot::delay
sub delay {
	my ($this) = @_;
	my $TR = $this->getTimer - time;
	return if $TR <= 0;
	warn "Sleeping for $TR seconds\n";
	sleep $TR;
}

sub run {
	my ($this) = @_;
	if (exists $this->{'bots'}) {
		die "Trying to run a " . __PACKAGE__ . " that is already running!\n";
	}
	$this->{'bots'} = $this->{'newbots'};
	$this->{'newbots'} = [];
	for my $bot ($this->bots) {
		$bot->init;
		$bot->onEv('begin');
	}
	while ($this->bots) {
		my @alive;
		my @dead;
		for my $bot ($this->bots) {
			$bot->step;
			if (my $cause = $bot->dead) {
				$bot->gracefulDeath($cause);
				push @dead, $bot;
			}
			else {
				push @alive, $bot;
			}
		}
		my @newborn = @{$this->{'newbots'}};
		$this->{'newbots'} = [];
		$this->{'bots'} = \@alive;
		my $botcount = scalar @alive;
		warn(
			$botcount . " bot" . (@alive == 1 ? '' : 's') . " currently living"
			. (@dead    ? ("; " . (scalar @dead   ) . " bot" . (@dead    == 1 ? '' : 's') . " just died"    ) : '')
			. (@newborn ? ("; " . (scalar @newborn) . " bot" . (@newborn == 1 ? '' : 's') . " newly started") : '')
		. "\n");
		my $limit = $this->limit;
		if (defined $limit and $limit < $botcount) {
			my $overage = $botcount - $limit;
			warn "We have exceeded limit of $limit; purging $overage\n";
			@alive = IRCBotInterp::sortScoreAge @alive;
			my $topscore = $alive[$#alive]->score;
			my $pmsg = "$botcount>$limit (ts:$topscore)";
			for my $i (1..$overage) {
				my $bot = shift @alive;
				$bot->purge($pmsg);
				$bot->gracefulDeath;
			}
			$this->{'bots'} = \@alive;
		}
		for my $bot (@newborn) {
			$bot->onEv('begin');
		}
		push @alive, @newborn;
		$this->delay;
	}
}

package IRCBotInterp::Bot;
use strict;
use warnings;

# IO::Socket is for IRC communications
use IO::Socket;
# Scalar::Util is for intelligent deep copy
use Scalar::Util qw(refaddr);

BEGIN {
	if ($have_hires) {
eval <<\PERL;
	use Time::HiRes qw(sleep time);
PERL
	}
}

my $isize = 4;

sub defaultCfg {
	# NOTE: this is a function to ensure arrays/hashes are new
(
	codepos => 0,
	registers => [
		undef,
		1..7,
		[@verInfo],
		9..15,
	],
	isize => $isize,
	movement => [$isize],
	idelay => 100,
	forkmode => 'process',
	eofmode  => 'random',
);
}

sub new {
	my $pkg = shift;
	my %args = defaultCfg;
	$args{id} = $IRCBotInterp::i = $IRCBotInterp::i + 1;
	if (scalar @_ == 1) {
		$args{'code'} = shift;
	}
	else {
		%args = (%args,
			@_
		);
	}
	my $this =
	bless \%args, $pkg;
	$this->onEv('create');
	$this
}

sub purge {
	my ($this, $reason) = @_;
	my $score = $this->score;
	if (defined $reason) {
		$reason = " (score:$score) $reason"
	}
	else {
		$reason = '';
	}
	$this->forceQuit("SOTFPURGE$reason");
	$this->{'dead'} = "PURGED $reason\n";
}

sub _deepcopy;
my %DCpy;
$DCpy{'ARRAY'} = sub {
	my $old = shift;
	my @new = @$old;
	
	our $DCc;
	my $oDCc = $DCc;
	my $i = 0;
	
	for my $e (@new) {
		$DCc = $oDCc . '->[' . $i . ']';
		$e = _deepcopy($e);
		++$i;
	}
	$DCc = $oDCc;
	\@new
};
$DCpy{'HASH'} = sub {
	my $old = shift;
	my %new = %$old;
	
	our $DCc;
	my $oDCc = $DCc;
	
	for my $k (keys %new) {
		$DCc = $oDCc . '->{\'' . $k . '\'}';
		$new{$k} = _deepcopy($new{$k});
	}
	$DCc = $oDCc;
	\%new
};
$DCpy{'CODE'} = sub {
	shift  # Hope for the best ;)
};
$DCpy{'IO::Socket::INET'} = sub {
	undef
};
sub _deepcopy {
	my ($o) = @_;
	my $r = ref $o;
	return $o if not defined $r;
	return $o unless length $r;
	
	my $addr = refaddr $o;
	our %DCd;
	our $DCc;
	if (exists $DCd{$addr}) {
		my $Dup = $DCd{$addr};
		my $rv = $Dup->[0];
		if (! defined $rv or refaddr $rv != $addr) {
			my $fc = $Dup->[1];
			my $msg = "Encountered multiple $o in deep copy";
			eval <<\EOC;
				use Carp;
				$msg = Carp::longmess $msg;
EOC
			$msg .= "\n" if $@;
			warn $msg;
			warn "First instance at $fc\n";
			warn " This instance at $DCc\n";
		}
		return $rv;
	}
	
	die "Don't know how to clone $r" unless exists $DCpy{$r};
	
	my $rv = $DCpy{$r}->($o);
	$DCd{$addr} = [
		$rv,
		$DCc,
	];
	$rv
}
sub _deepcopy_i {
	local our %DCd;
	local our $DCc = '';
	_deepcopy(@_)
}
sub dcp {
	# NOTE: Non-OO
	unless (wantarray) {
		return _deepcopy_i pop;
	}
	my @a = @_;
	for my $e (@a) {
		$e = _deepcopy_i($e);
	}
	(@a)
}

sub dataclone {
	my ($this) = @_;
	my $pkg = ref $this;
	my $args = dcp {%$this};
	bless $args, $pkg;
}

sub clone {
	my ($this) = @_;
	$this = $this->dataclone;
	$this->onEv('clone');
	$this
}

sub postmyfork {
	my ($this) = @_;
	$this->{id} = ++$IRCBotInterp::i;
	delete $this->{'socket'};
	$this->onEv('myfork');
}

sub myfork {
	my ($this) = @_;
	my $new;
	my $newid;
	my $forkmode = $this->{'forkmode'};
	if ('CODE' eq ref $forkmode) {
		($new, $newid) =
		$forkmode->($this);
	}
	elsif ($forkmode eq 'process') {
		my $FORK =
		fork;
		if (defined $FORK) {
			if ($FORK) {
				$newid = $FORK;
			}
			else {
			# Child process
			$new =
			our
			$Unique = $this;
			}
		}
	}
	elsif ($forkmode eq 'object') {
		$new =
		$this->clone;
	}
	elsif ($forkmode eq 'ignore') {
		return
	}
	else {
		warn __PACKAGE__ . " forkmode '$forkmode' unrecognized, ignoring fork\n";
	}
	if (defined $new) {
		$new->postmyfork;
		$newid = $new->nick if not defined $newid;
	}
	$newid
}

{
my $chars = '0123456789abcdefghijkLmnopqrstuvwxyz-[]\\`^';
my $cc = length $chars;
sub _genX_ {
	my ($i, $ml) = @_;
	--$i;
	my $rv = '';
	while (length $rv < $ml or $i) {
		$rv = (substr $chars, $i % $cc, 1) . $rv;
		$i = int $i / $cc;
	}
	$rv
}
sub _genX {
	my ($pid, $i) = @_;
	_genX_($pid, 3)
	. _genX_($i, 1)
}
}

sub nick {
	my ($this) = @_;
	my $nick = $this->{'nick'};
	my $i = $this->{'id'};
	if (not defined $nick) {
		return "SOTF-$$" if not $i;
		return "SOTF-" . _genX($$, $i);
	}
	elsif ('CODE' eq $nick) {
		return $nick->($this, $$, $i, \&_genX);
	}
	my $x = _genX($$, $i) if $nick =~ /\$x/;
	eval "\"$nick\"";
}

sub pStr {
	my ($this, $val) = @_;
	if (not defined $val) {
		$val = '';
	}
	elsif ('ARRAY' eq ref $val) {
		$val = "@$val";
	}
	$val
}

my %pCV;
$pCV{AA} = sub {
	# Both arrays, use input
	my ($a, $b, $num) = @_;
	if ($num) {
		(scalar @$a, scalar @$b, $num)
	}
	else {
		("@$a", "@$b", $num)
	}
};
$pCV{As} = sub {
	my ($a, $b) = @_;
	("@$a", $b, undef)
};
$pCV{An} = sub {
	my ($a, $b) = @_;
	(scalar @$a, $b, 1)
};
$pCV{Au} = sub {
	my ($a) = @_;
	(scalar @$a, 0, 1)
};
$pCV{sn} =
$pCV{ss} = sub {
	(@_[0,1], undef)
};
$pCV{su} = sub {
	($_[0], '', undef)
};
$pCV{nn} = sub {
	(@_[0,1], 1)
};
$pCV{nu} = sub {
	($_[0], 0, 1)
};
$pCV{uu} = sub {
	('', '', undef)
};
for my $gen (qw(sA nA ns uA us un)) {
	$gen =~ /(.)(.)/;
	my $inv = "$2$1";
	my $invF = $pCV{$inv};
	$pCV{$gen} = sub {
		$invF->(@_[1,0])
	};
}
sub prepComp {
	my ($a, $b, $num) = @_;
	my $types = '';
	for my $x ($a, $b) {
		if (not defined $x) {
			$types .= 'u';
		}
		elsif ('ARRAY' eq ref $x) {
			$types .= 'A';
		}
		elsif ($x =~ /\D/) {
			$types .= 's';
		}
		else {
			$types .= 'n';
		}
	}
	$pCV{$types}->($a, $b, $num);
}

sub isGr {
	my ($this, $a, $b) = @_;
	($a, $b, my $num) = prepComp($a, $b, 1);
	return $a > $b if $num;
	return $a gt $b;
}

sub isEq {
	my ($this, $a, $b) = @_;
	($a, $b, my $num) = prepComp($a, $b, 1);
	return $a == $b if $num;
	return $a eq $b;
}

sub rpBit {
	my ($this, $pp) = @_;
	my $bits = unpack 'B*', $$pp;
	$$pp = pack 'B*', substr $bits, 1;
	my $rv = substr $bits, 0, 1;
	warn "Reading param bit: $rv\n" if $this->{trace};
	$rv
}

our $nibbleName = 'nibble';
sub rpNibble {
	my ($this, $pp) = @_;
	my $p = unpack 'H*', $$pp;
	my $rv = hex substr $p, 0, 1;
	$$pp = pack 'H*', substr $p, 1;
	warn "Reading param $nibbleName: $rv\n" if $this->{trace};
	$rv
}

sub rp16 {
	my ($this, $pp) = @_;
	my $rv = hex unpack 'H*', substr $$pp, 0, 2;
	$$pp = substr $$pp, 2;
	warn "Reading param 16-bit: $rv\n" if $this->{trace};
	$rv
}

sub rpOctet {
	my ($this, $pp) = @_;
	my $rv = hex unpack 'H*', substr $$pp, 0, 1;
	$$pp = substr $$pp, 1;
	warn "Reading param octet: $rv\n" if $this->{trace};
	$rv
}

sub rpSOctet {
	my ($this, $pp) = @_;
	my $rv = hex unpack 'H*', substr $$pp, 0, 1;
	$$pp = substr $$pp, 1;
	$rv = -(256 - $rv) unless $rv < 128;
	warn "Reading param signed octet: $rv\n" if $this->{trace};
	$rv
}

sub rpASCII {
	my ($this, $pp) = @_;
	my $rv = chr hex unpack 'H*', substr $$pp, 0, 1;
	$$pp = substr $$pp, 1;
	warn "Reading param ASCII character: $rv\n" if $this->{trace};
	$rv
}

sub rpMChange {
	my ($this, $pp) = @_;
	my $rv = hex unpack 'H*', substr $$pp, 0, 1;
	$$pp = substr $$pp, 1;
	$rv = ((int $rv / 128) ? '+' : '-') . chr($rv % 128);
	warn "Reading param mode change: $rv\n" if $this->{trace};
	$rv
}

sub rpReg {
	my $this = shift;
	local $nibbleName = 'register';
	$this->
	rpNibble(@_)
	% 16
}

sub rpRegV {
	my ($this, $pp) = @_;
	my $reg = rpReg(@_);
	$this->getRegister($reg);
}
sub rpRegA {
	my ($this, $pp) = @_;
	my $reg = rpReg(@_);
	$this->getRegisterA($reg);
}

my $null;
sub getRegister {
	my ($this, $reg) = @_;
	if (not $reg) {
		my $null
	}
	else {
		$this->{'registers'}->[$reg]
	}
}
sub getRegisterA {
	my ($this, $reg) = @_;
	if (not $reg) {
		[]
	}
	else {
		my $val = $this->getRegister($reg);
		if ('ARRAY' ne ref $val) {
			$this->setRegister($reg, $val = [$val]);
		}
		$val
		# NOTE: trivial to make lvalue-compliant, but if you want an lvalue with this func, you're doing something wrong :P
	}
}
sub setRegister {
	my ($this, $reg, $val) = @_;
	warn("Register $reg set to: " . $this->pStr($val) . "\n") if $this->{trace};
	$this->{'registers'}->[$reg] = $val;
}

sub getCallStack {
	my ($this) = @_;
	$this->{'callstack'} ||= [];
}

sub callFunc {
	my ($this, $op) = @_;
	my $func = substr $op, -2;
	$func = (hex $func) % 128;
	my $fdata = $this->{'functions'}->[$func];
	my $CS = $this->getCallStack;
	push @$CS, $this->{'codepos'};
	$this->{'codepos'} = $fdata->{'codepos'};
}

sub _makeValidOffset {
	my ($this, $offset, $size) = @_;
	$size = 0 unless defined $size;
	if ($offset < 0) {
		# Move the entire program to make offset 0
		my $pplen = abs $offset;
		$this->{'code'} = (chr(0) x $pplen) . $this->{'code'};
		$this->{'codepos'} += $pplen;
		for my $F (@{$this->{'functions'}}) {
			next unless defined $F;
			$F->{'codepos'} += $pplen;
		}
		$offset = 0;
	}
	my $codelen = length $this->{'code'};
	my $endoffset = $offset + $size;
	if ($endoffset > $codelen) {
		$this->{'code'} .= chr(0) x ($endoffset - $codelen);
	}
	$offset
}

my %Ops;
$Ops{''} = sub {};
$Ops{'000f'} = sub {  # function return
	my ($this, $op, $pd) = @_;
	my $CS = $this->getCallStack;
	$this->{'codepos'} = pop @$CS;
};
$Ops{'0010'} = sub {
	my ($this, $op, $pd) = @_;
	my $xic = $this->rpNibble(\$pd);
	my $sic = $this->rpNibble(\$pd);
	my $isize = $this->{isize};
	$this->{'movement'} = [
		# TODO: Verify this pattern :)
		($isize) x ($xic - 1),
		($isize * ($sic + 1)),
	];
};
$Ops{'0011'} = sub {
	my ($this) = @_;
	$Ops{'0010'}->(@_);
	$this->{'movement'} = [
		map {
			-$_
		}
		@{$this->{'movement'}}
	]
};
$Ops{'0020'} = sub {
	my ($this, $op, $pd) = @_;
	my $skip = $this->rpNibble(\$pd);
	$this->{codepos} += $skip * $this->{'isize'};
};
$Ops{'0021'} = sub {
	my ($this, $op, $pd) = @_;
	my $skip = $this->rpNibble(\$pd);
	$this->{codepos} -= $skip * $this->{'isize'};
};
$Ops{'002f'} = sub {
	my ($this) = @_;
	my $op = '0021';
	if ($this->{'movement'}->[0] > 0) {
		$op = '0020';
	}
	$Ops{$op}->(@_);
};
$Ops{'003'} = sub {
	my ($this, $op, $pd) = @_;
	my $LHS = $this->rpRegV(\$pd);
	my $RHS = $this->rpRegV(\$pd);
	my $cop = unpack 'B*', pack 'H*', substr $op, 3, 1;
	my $Gr = substr $cop, 0, 1;
	my $Eq = substr $cop, 1, 1;
	my $Pos = substr $cop, 3, 1;
	my $TRUTH;
	$TRUTH = 1 if $Gr and $this->isGr($LHS, $RHS);
	$TRUTH = 1 if $Eq and $this->isEq($LHS, $RHS);
	$TRUTH = not $TRUTH unless $Pos;
	return unless $TRUTH;
	
	my $skip = $this->rpOctet(\$pd);
	$skip = -$skip if ($this->{'movement'}->[0] < 0);
	$this->{'codepos'} += $skip * $this->{'isize'};
};
$Ops{'00f0'} = sub {
	my ($this, $op, $pd) = @_;
	my $sec = $this->rpNibble(\$pd);
	$this->{'nexttime'} = time + $sec;
};
$Ops{'00f1'} = sub {
	my ($this, $op, $pd) = @_;
	$this->{'idelay'} = $this->rp16(\$pd);
};
$Ops{'020'} =
$Ops{'021'} =
$Ops{'022'} =
$Ops{'023'} =
sub {
	my ($this, $op, $pd) = @_;
	my $func = hex substr $op, -2;
	my $fdata = {};
	$fdata->{'codepos'} = $this->{'codepos'};
	$this->{'functions'}->[$func] = $fdata;
	my $fcall = sprintf '%02x', 128 + $func;
	$Ops{$fcall} = \&callFunc;
	
	my $skip = $this->rpOctet(\$pd);
	$skip *= $this->{'isize'};
	$this->{'codepos'} += $skip;
};
$Ops{'0800'} = sub {
	my ($this, $op, $pd) = @_;
	my $tgt = $this->rpReg(\$pd);
	my $mode = $this->rpNibble(\$pd);
	my $nv;
	if ($mode == 15) {
		$nv = $this->rpOctet(\$pd);
	}
	elsif ($mode) {
		$nv = rand;
	}
	$this->setRegister($tgt, $nv);
};
$Ops{'0810'} = sub {
	my ($this, $op, $pd) = @_;
	my $val = $this->rpRegV(\$pd);
	my $reg = $this->rpReg(\$pd);
	$val = dcp $val;
	$this->setRegister($reg, $val);
};
$Ops{'0813'} = sub {
	my ($this, $op, $pd) = @_;
	my $arr = $this->rpRegA(\$pd);
	my $dst = $this->rpReg(\$pd);
	my $elem = $this->rpOctet(\$pd);
	my $val = $arr->[$elem];
	$val = dcp $val;
	$this->setRegister($dst, $val);
};
$Ops{'0818'} = sub {
	my ($this, $op, $pd) = @_;
	my $val = $this->rpRegV(\$pd);
	my $arr = $this->rpRegA(\$pd);
	$val = dcp $val;
	push @$arr, $val;
};
$Ops{'0819'} = sub {
	my ($this, $op, $pd) = @_;
	my $val = $this->rpRegV(\$pd);
	my $arr = $this->rpRegA(\$pd);
	$val = dcp $val;
	unshift @$arr, $val;
};
$Ops{'081a'} = sub {
	my ($this, $op, $pd) = @_;
	my $arr = $this->rpRegA(\$pd);
	my $reg = $this->rpReg(\$pd);
	my $val = pop @$arr;
	$val = dcp $val;
	$this->setRegister($reg, $val);
};
$Ops{'081b'} = sub {
	my ($this, $op, $pd) = @_;
	my $arr = $this->rpRegA(\$pd);
	my $reg = $this->rpReg(\$pd);
	my $val = shift @$arr;
	$val = dcp $val;
	$this->setRegister($reg, $val);
};
$Ops{'0820'} = sub {
	my ($this, $op, $pd) = @_;
	my $reg = $this->rpReg(\$pd);
	my $max = $this->rpNibble(\$pd) || undef;
	my $char = chr $this->rpOctet(\$pd);
	my $val = $this->getRegister($reg);
	$this->setRegister($reg, (split /\Q$char\E/, $val, $max));
};
$Ops{'0821'} = sub {
	my ($this, $op, $pd) = @_;
	my $reg = $this->rpReg(\$pd);
	my $prepend = $this->rpBit(\$pd);
	my $append = $this->rpBit(\$pd);
	my $literal = $this->rpBit(\$pd);
	$this->rpBit(\$pd);
	my $joiner;
	if ($literal) {
		$joiner = chr $this->rpOctet(\$pd);
	}
	else {
		$joiner = $this->rpRegV(\$pd);
	}
	my @arr = @{$this->getRegisterA($reg)};
	unshift @arr, '' if $prepend;
	push @arr, '' if $append;
	$this->setRegister($reg, (join $joiner, @arr));
};
$Ops{'08f0'} = sub {  # NOTE: inline scalar literal
	my ($this, $op, $pd) = @_;
	my $dst = $this->rpReg(\$pd);
	$this->rpBit(\$pd);
	$this->rpBit(\$pd);
	$this->rpBit(\$pd);
	my $pad = $this->rpBit(\$pd);
	my $size = $this->rpOctet(\$pd);
	my $isize = $this->{'isize'};
	my $rsize = $pad ? ($isize * int(($size + ($isize - 1)) / $isize)) : $size;
	my $offset = $this->{'codepos'} + $isize;
	my $literal = substr $this->{'code'}, $offset, $size;
	$this->{'codepos'} += $rsize;
	warn "Literal '$literal' into register $dst\n" if $this->{trace};
	$this->setRegister($dst, $literal);
};
my %ahGen = (
	'r' => 'RegV',
	'm' => 'MChange',
	'c' => 'ASCII',
	'o' => 'Octet',
);
for my $adhoc (
	'3001:NICK %r1',
	'3005:QUIT :%r1',
	'3100:JOIN %r1 :%r2',
	'3101:PART %r1',
	'3102:MODE %r1 %m3 :%r2',
	'3103:TOPIC %r1 :%r2',
	'3104:NAMES %r1',
	'3105:LIST %r1',
	'3106:INVITE %r1 %r2',
	'3107:KICK %r1 %r2 :%r3',
	'3200:VERSION',
	'3201:STATS %c1',
	'3300:PRIVMSG %r1 :%r2',
	'3301:NOTICE %r1 :%r2',
	'3400:WHO %r1 %c2',
	'3401:WHOIS %r1 :%r2',
	'3402:WHOWAS %r1 %o2',
) {
	my ($op, $st) = ($adhoc =~ m/^([\da-f]{4})\:(.*)$/);
	my $opc = <<\EOC;
sub {
	my ($this, $op, $pd) = @_;
	my $sock = $this->{'socket'};
	return unless defined $sock;
EOC
	for my $n (1..3) {
		$st =~ s/\%((.)$n)/\$$1/ or last;
		my ($vn, $g) = ($1, $2);
		$opc .= "my \$$vn = \$this->pStr(\$this->rp$ahGen{$2}(\\\$pd));\n";
	}
	$opc .= <<EOC;
		warn "IRC: $st\n" if \$this->{trace};
		print \$sock "$st\\r\\n";
	};
EOC
	$Ops{$op} = eval $opc;
}
{
my %AllowedCommands = (map { $_ => undef } qw(
	NICK QUIT
	JOIN PART MODE TOPIC NAMES LIST INVITE KICK
	VERSION STATS
	PRIVMSG NOTICE
	WHO WHOIS WHOWAS
));
$Ops{'7000'} = sub {  # Raw command
	my ($this, $op, $pd) = @_;
	my $cmd = $this->rpRegV(\$pd);
	return unless exists $AllowedCommands{$cmd};
	my $sock = $this->{'socket'};
	return unless defined $sock;
	while (length $pd) {
		my $reg = $this->rpReg(\$pd);
		last unless $reg;
		
		my $arg = $this->getRegister($reg);
		last unless defined $arg;
		
		$arg = ":$arg" if $arg =~ /\s/;
		$cmd .= " $arg";
	}
	print $sock "$cmd\r\n";
};
}
$Ops{'70f0'} = sub {  # Establish connection
	my ($this, $op, $pd) = @_;
	my $sock = new IO::Socket::INET(
		PeerAddr => 'irc.mkdir.name',
		PeerPort => 6667,
		Proto => 'tcp',
	);
	return if not defined $sock;  # FIXME
	my $nick = $this->nick;
	print $sock "NICK $nick\r\n";
	my $realname = $this->{'realname'} || 'None';
	print $sock "USER $nick 8 * :$realname\r\n";
	$this->{'nexttime'} = time + 1;  # FIXME
	$this->score(1);
	$this->{'socket'} = $sock;
};
$Ops{'70f1'} = sub {  # Close connection
	my ($this, $op, $pd) = @_;
	my $sock = $this->{'socket'};
	return unless defined $sock;
	my $pdx = unpack 'H*', $pd;
	$this->forceQuit("SOTFCLOSE $op$pdx");
};
$Ops{'f000'} = sub {  # suicide
	my ($this, $op, $pd) = @_;
	$this->{'dead'} = "I killed myself.\n";
};
$Ops{'f0f0'} = sub {  # fork
	my ($this, $op, $pd) = @_;
	
	my $remaining = $this->rpNibble(\$pd);
	return if not $remaining;
	my $rpos = $this->{'codepos'} + 2;
	my $rctx = unpack 'H*', substr $this->{'code'}, $rpos, 1;
	--$remaining;
	substr($rctx, 0, 2) = sprintf '%x', $remaining;
	substr($this->{'code'}, $rpos, 1) = $rctx;
	
	my $score = $this->score;
	my $sused = $this->{'forks'} || 0;
	return unless $sused < $score;
	
	++$this->{'forks'};
	warn "Fork! Remaining: $remaining; Score used: $sused / $score\n" if $this->{trace};
	my $FORK = $this->myfork;
	return unless defined $FORK;
	$this->setRegister(15, $FORK);
};
$Ops{'f100'} = sub {  # block modification
	my ($this, $op, $pd) = @_;
	my $offset = $this->rpSOctet(\$pd);
	my $size = $this->rpNibble(\$pd);
	my $vary = $this->rpNibble(\$pd);
	my $isize = $this->{'isize'};
	
	$offset *= $isize;
	$offset += $this->{'codepos'};
	$size *= $isize;
	
	$offset = $this->_makeValidOffset($offset, $size);
	my $block = substr $this->{'code'}, $offset, $size;
warn("SIZE: $size ; OFFSET $offset ; blockLen ".(length $block)."\n");
	
	# 2 bits randomized per variance level
	$vary *= 2;
	for (my $i = 0; $i < $size; $i += $isize) {
		my $ivary = $vary + int((rand(3.3)-1.65)**7);
		next if $ivary <= 0;
		if ($ivary > 24) {
			# Just generate a new instruction
			substr($block, $i, $isize) = $this->randInstruction;
			next;
		}
		my $bits = unpack 'B*', substr $block, $i, $isize;
		my $novary = 32 - $ivary;
		my $rbits = '';
		$rbits .= int(rand(2)) for 1..$ivary;
warn("BITS: ".(length $bits)." ; rBits ".(length $rbits)." ; nv $novary ; iv $ivary ; i $i\n");
		substr($bits, $novary, $ivary) = $rbits;
		substr($block, $i, $isize) = pack 'B*', $bits;
	}
	
	substr($this->{'code'}, $offset, $size) = $block;
};
$Ops{'f101'} = sub {  # block insert
	my ($this, $op, $pd) = @_;
	my $offset = $this->rpSOctet(\$pd);
	my $size = $this->rpNibble(\$pd);
	my $isize = $this->{'isize'};
	
	$offset *= $isize;
	$offset += $this->{'codepos'};
	$size *= $isize;
	
	$offset = $this->_makeValidOffset($offset);
	
	my $data = '';
	$data .= $this->randInstruction for 1..$size;
	
	substr($this->{'code'}, $offset, 0) = $data;
};
$Ops{'f111'} = sub {  # block delete
	my ($this, $op, $pd) = @_;
	# TODO
};
$Ops{'f180'} = sub {  # size fuzzy factor
	my ($this, $op, $pd) = @_;
	# TODO
};
$Ops{'f20'} = sub {
	my ($this, $op, $pd) = @_;
	# TODO
};

delete $Ops{''};
for my $k (keys %Ops) {
	next if 4 == length $k;
	my $code = $Ops{$k};
	for my $sfx (0..9, qw(a b c d e f)) {
		$Ops{"$k$sfx"} = $code;
	}
	delete $Ops{$k};
}

my @Ops = keys %Ops;
warn ("Total opcodes: " . (scalar @Ops) . "\n");

sub randInstruction {
	my ($this) = @_;
	my $op = $Ops[$[ + int rand scalar @Ops];
	$op = pack 'H*', $op;
	my $rv = $op;
	$rv .= chr int rand 256 for 1..2;
	$rv
}

my %SubCTCP;
my $BootCommit =
`git log -1 --pretty=oneline -- "$INC{'IRCBotInterp.pm'}" 2>&1`;
$SubCTCP{COMMIT} = sub {
	$BootCommit
};
$SubCTCP{ENV} = sub {
	my ($this, $vn) = @_;
	$ENV{$vn}
};
$SubCTCP{HOSTNAME} = sub {
	eval <<\EOC || $@;
	use Sys::Hostname;
	hostname;
EOC
};
$SubCTCP{USERNAME} = sub {
	$ENV{'LOGNAME'} || $ENV{'USER'};
};
$SubCTCP{VERSION} = sub {
	"@verInfo";
};

my %hasTarget = map { $_ => 1 } qw(
	JOIN PART MODE TOPIC NAMES LIST INVITE KICK
	PRIVMSG NOTICE
	WHO WHOIS WHOWAS KILL
);
sub doNetwork {
	my ($this) = @_;
	my $sock = $this->{'socket'};
	return if not defined $sock;
=pod Skippable with non-blocking mode
	{
		my $r = '';
		vec($r, $sock->fileno, 1) = 1;
		my $ready = select $r, undef, undef, 0;
		return unless $ready;
	}
=cut
	while (1) {
		$sock->blocking(0);
		my $line = <$sock>;
		$sock->blocking(1);
		if (not defined $line) {
			if (not $sock->connected) {
				$this->score(-1);
			}
			last
		}
		
		chomp $line;
		
		if ($line =~ /^(?:\:(\S+)\s+)?(\S+)(?:\s+(.*?))?\r?\n?$/) {
			my ($from, $cmd, $params) = ($1, $2, $3);
			
			if ($cmd eq 'PING') {
				print $sock "PONG :$params\r\n";
				next
			}
			
			my $to;
			# Valid IRC input :)
			my $fromshort;
			($fromshort) = ($from =~ /^([^!]+)\!/) if defined $from;
			if (defined $params) {
				$params =~ s/(?:^|\s+)\:(.*)$//;
				my ($finalparam) = ($1);
			$params = [split /\s+/, $params];
			push @$params, $finalparam if defined $finalparam;
				$to = shift @$params if $hasTarget{$cmd};
			if (@$params == 1) {
				$params = $params->[$[];
			}
			}
			
			my $nick = $this->nick;
			if ($cmd eq 'PRIVMSG' and $params =~ /\Q$nick\E/) {
				if ($params =~ /\-\-/) {
					$this->score(-1);
				}
				elsif ($params =~ /\+\+/) {
					$this->score(1);
				}
			}
			
			if ($cmd eq 'PRIVMSG' and not ref $params and "\x01" eq substr($params, 0, 1)) {
				substr($params, -1) = '' if substr($params, -1) eq "\x01";
				$params = [split /\s+/, (substr $params, 1)];
				$cmd = "CTCP-" . (shift @$params);
				$params = $params->[$[] if @$params == 1;
				
				if ($cmd =~ /^CTCP\-(SOTF(.*))/) {
					my ($ctcp, $type) = ($1, $2);
					# Subconcious info
					my $reply;
					my $Subcode = $SubCTCP{$type};
					$reply = $Subcode->($this, $params) if defined $Subcode;
					$reply = "(Unknown)" if not defined $reply;
					print $sock "NOTICE $fromshort :\x01$ctcp $reply\x01\r\n";
				}
			}
			
			$this->setRegister(11, $from);
			$this->setRegister(12, $fromshort);
			$this->setRegister(13, $to);
			$this->setRegister(14, $cmd);
			$this->setRegister(15, $params);
		}
		else {
			$this->setRegister(14, undef);
			$this->setRegister(15, $line);
		}
		last;  # give bot a LITTLE time to process :)
	}
}

sub init {
	my ($this) = @_;
	$this->onEv('preinit');
	$this->{'starttime'} = time;
	$this->{'icount'} = 0;
	$this->{'nexttime'} = time;
}

sub startTime {
	my ($this) = @_;
	$this->{'starttime'}
}

sub step {
	my ($this) = @_;
	
	{
		my $now = time;
		return if $this->{'nexttime'} > $now;
		$this->{'nexttime'} = time + ($this->{'idelay'} / 1000)
	}
	$this->doNetwork;
	
	++$this->{'icount'};
	my $cpos = $this->codepos;
	my $ins = substr $this->{code}, $cpos, 4;
	my $op = unpack 'H*', substr $ins, 0, 2;
	# NOTE: Moved register manipulation from 01 to 08...
	substr($op, 0, 2) = '08' if '01' eq substr $op, 0, 2;
	my $lop = $op;
	my $opCode;
	while (1) {
		$opCode = $Ops{$lop};
		last if defined $opCode;
		last unless length $lop;
		$lop = substr $lop, 0, -1;
	}
	if (defined $opCode) {
		my $pd = substr $ins, 2;
		if ($this->{'trace'}) {
			warn("Calling op $op" . (($op ne $lop) ? " via $lop" : '') . " code (at " . sprintf('%x', $cpos) . ")\n");
		}
		$opCode->($this, $op, $pd);
	}
	my $tm;
	for my $m ($this->{movement}) {
		push @$m, $tm = shift @$m;
	}
	$this->{codepos} += $tm;
}

sub onEv {
	my ($this, $event) = @_;
	my $oevent = $event;
	$event =~ s/^(.)/'on' . uc $1/e;
	$event = $this->{$event};
	return unless defined $event;
	$event = [$event] if 'ARRAY' ne ref $event;
	for my $f (@$event) {
		$f->($this, $oevent);
	}
}

sub getTimer {
	my ($this) = @_;
	$this->{'nexttime'}
}

sub delay {
	my ($this) = @_;
	my $TR = $this->getTimer - time;
	return if $TR <= 0;
	sleep $TR;
}

sub codepos {
	my ($this) = @_;
	my $CP = $this->{'codepos'};
	my $CL = length $this->{'code'};
	
	if ($CP > $CL or $CP < 0) {
		my $problem = "I got too far ahead of myself...\n";
		my $mode = $this->{'eofmode'};
		
		if ($mode eq 'kill') {
			$this->{'dead'} = $problem;
		}
		else {
			warn $problem;
			
			if ($mode eq 'reset') {
				$CP = 0;
			}
			else {
				
		my $isize = $this->{'isize'};
		$CP = int(rand($CL) / $isize) * $isize;
				
			}
		$this->{'codepos'} = $CP;
		
		warn("Jumping to " . sprintf('%x', $CP) . "!\n");
		
		}
	}
	
	$CP
}

sub score {
	my ($this, $change) = @_;
	my $score = $this->{'score'};
	if (not defined $score) {
		$score =
		$this->{'score'} = 0;
	}
	if (defined $change) {
		$score =
		$this->{'score'} += $change;
	}
	$score
}

sub dead {
	my ($this) = @_;
	my $codepos = $this->codepos;
	if ($this->{'dead'}) {
		return $this->{'dead'};
	}
	if ($this->{'movement'}->[0] == 0 or substr($this->{'code'}, $codepos, 3) eq "\0\x21\x01") {
		return 'My brain has stopped...';
	}
	our $Unique;
	if (defined $Unique and $Unique != $this) {
		if (defined (my $sock = $this->{'socket'})) {
			delete $this->{'socket'};
		}
		return 'I was never meant to be...';
	}
}

sub forceQuit {
	my ($this, $msg) = @_;
	my $sock = $this->{'socket'};
	return unless defined $sock;
	
	print $sock "QUIT :$msg\r\n";
	$sock->flush;
	$sock->close;
	undef $this->{'socket'};
}

sub gracefulDeath {
	my ($this, $cause) = @_;
	
	$cause = $this->dead unless defined $cause;
	
	warn $cause;
	
	$this->onEv('death');
	
	$this->forceQuit("SOTFDEATH $cause");
}

sub run {
	my ($this) = @_;
	$this->init;
	$this->onEv('begin');
	while (1) {
		$this->step;
		$this->delay;
		# Failsafes:
		if (my $cause = $this->dead) {
			$this->gracefulDeath($cause);
			last
		}
	}
}

sub dump {
	my ($this) = @_;
	my $out = '';
	my %copy = %$this;
	$copy{code} = 'Post-Dump';
	eval <<\EOC;
		use Data::Dumper;
		$out .= Dumper(\%copy);
EOC
	$out .= $this->{'code'};
	$out
}
